package com.example.firstSpring;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
@EnableAutoConfiguration
public class GreetingController {
    @ResponseBody
    @GetMapping("/")
    public String greeting() {
        return "<h1 align=center>Hello, world!</h1>";
    }
}
