package com.yukon.library.model.order;

import com.yukon.library.api.order.OrderDTOGetResponse;
import com.yukon.library.api.order.OrderDTOSaveRequest;
import com.yukon.library.model.book.*;
import com.yukon.library.model.client.Client;
import com.yukon.library.model.client.ClientNotFoundException;
import com.yukon.library.model.client.ClientRepository;
import com.yukon.library.model.client.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.CustomEditorConfigurer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {
    @Autowired
    private OrderRepository repository;

    @Autowired
    private BookService bookService;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ClientRepository clientRepository;

    public List<OrderDTOGetResponse> getOrdersByStatus(OrderStatus status) {
        return repository.findAll().stream()
                .filter(order -> order.getStatus() == status)
                .map(OrderDTOGetResponse::new)
                .collect(Collectors.toList());
    }

    public List<OrderDTOGetResponse> findAll() {
        return repository.findAll().stream()
                .map(OrderDTOGetResponse::new)
                .collect(Collectors.toList());
    }

    public OrderDTOGetResponse findById(Long id) {
        return repository.findById(id)
                .map(OrderDTOGetResponse::new)
                .orElseThrow(() -> new OrderNotFoundException(id));
    }

    public OrderDTOGetResponse save(OrderDTOSaveRequest orderDTO) throws BookNotAvailableException {
        Orders order  = new Orders();
        Book book = bookRepository.findById(orderDTO.getBookID())
                .orElseThrow(() -> new BookNotFoundException(orderDTO.getBookID()));
        Client client = clientRepository.findById(orderDTO.getCustomerID())
                .orElseThrow(() -> new ClientNotFoundException(orderDTO.getCustomerID()));

        order.setBook(book);
        order.setClient(client);

        if(bookService.isAvailable(order.getBook()))
            repository.save(order);
        else
            throw new BookNotAvailableException(order.getBook().getId());
        return new OrderDTOGetResponse(order);
    }

    public OrderDTOGetResponse update(OrderDTOSaveRequest orderDTO, Long id) throws BookNotAvailableException {

        Book book = bookRepository.findById(orderDTO.getBookID())
                .orElseThrow(() -> new BookNotFoundException(orderDTO.getBookID()));


        Client client = clientRepository.findById(orderDTO.getCustomerID())
                .orElseThrow(() -> new ClientNotFoundException(orderDTO.getCustomerID()));

        if(repository.existsById(id)) {
            Orders order = repository.findById(id).get();
            if(order.getBook()!=book && !bookService.isAvailable(book))
                throw new BookNotAvailableException(book.getId());

            order.setClient(client);
            order.setBook(book);
            order.setStatus(orderDTO.getOrderStatus());

            return new OrderDTOGetResponse(repository.save(order));
        }

        if(!bookService.isAvailable(book))
            throw new BookNotAvailableException(book.getId());

        Orders order = new Orders(client, book);
        order.setId(id);

        return new OrderDTOGetResponse(repository.save(order));
    }

    public void delete(Long id) {
        if(repository.existsById(id))
            repository.deleteById(id);
        else
            throw new OrderNotFoundException(id);
    }

    public OrderDTOGetResponse setStatus(Long id, OrderStatus status) {
        return repository.findById(id)
                .map(order -> {
                    order.setStatus(status);
                    return new OrderDTOGetResponse(repository.save(order));
                })
                .orElseThrow(() -> new OrderNotFoundException(id));
    }

}
