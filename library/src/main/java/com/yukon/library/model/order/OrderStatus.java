package com.yukon.library.model.order;

public enum OrderStatus{
    FINISHED,
    IN_PROCESS
}
