package com.yukon.library.model.book;

public class BookNotAvailableException extends Exception {
    public BookNotAvailableException(Long id) {
        super("Book " + id + " is not available" );
    }
}
