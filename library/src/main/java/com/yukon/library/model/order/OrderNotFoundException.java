package com.yukon.library.model.order;

public class OrderNotFoundException extends RuntimeException {
    public OrderNotFoundException(Long id) {
        super("Can not found book " + id);
    }
}
