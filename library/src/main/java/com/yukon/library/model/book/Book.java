package com.yukon.library.model.book;



import com.yukon.library.api.book.BookDTO;
import com.yukon.library.model.order.Orders;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String bookName;
    private String authorName;
    private int copiesCount;


    @OneToMany(mappedBy = "book",
                cascade = CascadeType.ALL,
                orphanRemoval = true)
    private List<Orders> orders;


    public Book() {}

    public Book(String bookName, String authorName, int copiesCount) {
        this.bookName = bookName;
        this.authorName = authorName;
        this.copiesCount = copiesCount;
    }

    public Book(BookDTO book) {
        if(book.getId()!=null) {
            this.setId(book.getId());
        }

        this.bookName = book.getBookName();
        this.copiesCount = book.getCopiesCount();
        this.authorName = book.getAuthorName();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }


    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }


    public int getCopiesCount() {
        return copiesCount;
    }

    public void setCopiesCount(int copiesCount) throws IllegalArgumentException {
        if(copiesCount < 0)
            throw new IllegalArgumentException("Invalid copies count");
        this.copiesCount = copiesCount;
    }


    public List<Orders> getOrders() {
        return this.orders;
    }

    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }


    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", authorName='" + authorName + '\'' +
                ", copiesCount=" + copiesCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(id, book.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
