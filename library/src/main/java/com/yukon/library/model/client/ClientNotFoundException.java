package com.yukon.library.model.client;

public class ClientNotFoundException extends RuntimeException {
    public ClientNotFoundException(Long id) {
        super("Can not found book " + id);
    }
}
