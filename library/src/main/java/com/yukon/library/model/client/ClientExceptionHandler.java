package com.yukon.library.model.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ClientExceptionHandler {

    @ResponseBody
    @ExceptionHandler(ClientNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String BookNotFoundExceptionHandler(ClientNotFoundException ex) {
        return ex.getMessage();
    }
}
