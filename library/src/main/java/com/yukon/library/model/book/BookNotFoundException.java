package com.yukon.library.model.book;

public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(Long id) {
        super("Can not found book " + id);
    }
}
