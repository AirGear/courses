package com.yukon.library.model.order;


import com.yukon.library.model.book.Book;
import com.yukon.library.model.client.Client;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private OrderStatus status;

    @ManyToOne
    private Client client;

    @ManyToOne
    private Book book;


    public Orders() {}

    public Orders(Client client, Book book) {
        this.client = client;
        this.book = book;
        status = OrderStatus.IN_PROCESS;
    }

    public Orders(Client client, Book book, OrderStatus status) {
        this.client = client;
        this.book = book;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }


    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orders order = (Orders) o;
        return id.equals(order.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", client=" + client +
                ", book=" + book +
                '}';
    }
}
