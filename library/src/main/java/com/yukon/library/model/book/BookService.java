package com.yukon.library.model.book;

import com.yukon.library.api.book.BookDTO;
import com.yukon.library.api.client.ClientDTO;
import com.yukon.library.model.order.OrderStatus;
import com.yukon.library.model.order.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {
    @Autowired
    private BookRepository repository;

    public boolean isAvailable(Book book) {
        int soldCopies = 0;

        List<Orders> orders  = book.getOrders();
        for(Orders order : orders) {
            if(order.getStatus() == OrderStatus.IN_PROCESS)
                soldCopies++;
        }
        return book.getCopiesCount() > soldCopies;
    }

    public List<ClientDTO> getClientsByBook(Long id, OrderStatus status) {
        Book book = repository.findById(id)
                .orElseThrow(() -> new BookNotFoundException(id));

        return book.getOrders().stream()
                .filter(order -> order.getStatus() == status)
                .map(order -> new ClientDTO(order.getClient()))
                .distinct()
                .collect(Collectors.toList());
    }

    public List<ClientDTO> getClientsByBook(Long id) {
        Book book = repository.findById(id)
                .orElseThrow(() -> new BookNotFoundException(id));

        return book.getOrders().stream()
                .map(order -> new ClientDTO(order.getClient()))
                .distinct()
                .collect(Collectors.toList());
    }

    public BookDTO findById(Long id) {
        return repository.findById(id)
                .map(BookDTO::new)
                .orElseThrow(() -> new BookNotFoundException(id));
    }

    public List<BookDTO> findAll() {
        return repository.findAll()
                .stream()
                .map(BookDTO::new)
                .collect(Collectors.toList());
    }

    public BookDTO save(BookDTO bookDTO) {
        Book book = new Book(bookDTO);
        repository.save(book);
        return new BookDTO(book);
    }

    public BookDTO update(BookDTO bookDTO, Long id) {
        return repository.findById(id)
                .map(book -> {
                    book.setBookName(bookDTO.getBookName());
                    book.setAuthorName(bookDTO.getAuthorName());
                    book.setCopiesCount(bookDTO.getCopiesCount());
                    return new BookDTO(repository.save(book));
                })
                .orElseGet(() ->{
                    Book book = new Book(bookDTO);
                    book.setId(id);
                    return new BookDTO(repository.save(book));
                });
    }


    public void deleteById(Long id) {
        if(repository.existsById(id))
            repository.deleteById(id);
        else
            throw new BookNotFoundException(id);
    }
}

