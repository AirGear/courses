package com.yukon.library.model.client;


import com.yukon.library.api.book.BookDTO;
import com.yukon.library.api.client.ClientDTO;
import com.yukon.library.model.order.OrderRepository;
import com.yukon.library.model.order.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private OrderRepository orderRepository;


    public List<BookDTO> getBooksByClient(Long id) {
        Client client = clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(id));

        return client.getOrders().stream()
                .map(order -> new BookDTO(order.getBook()))
                .collect(Collectors.toList());
    }

    public List<BookDTO> getBooksByClient(Long id, OrderStatus status) {
        Client client = clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(id));

        return client.getOrders().stream()
                .filter(order -> order.getStatus()==status)
                .map(order -> new BookDTO(order.getBook()))
                .collect(Collectors.toList());
    }

    public List<ClientDTO> getActiveClients() {
        return orderRepository.findAll().stream()
                .filter(order -> order.getStatus()==OrderStatus.IN_PROCESS)
                .map(order -> new ClientDTO(order.getClient()))
                .distinct()
                .collect(Collectors.toList());
    }

    public List<ClientDTO> getPassiveClients() {
        List<ClientDTO> clients = this.findAll();
        clients.removeAll(getActiveClients());
        return clients;
    }


    public ClientDTO findById(Long id) {
        return clientRepository.findById(id)
                .map(ClientDTO::new)
                .orElseThrow(() -> new ClientNotFoundException(id));
    }

    public List<ClientDTO> findAll() {
        return clientRepository.findAll().stream()
                .map(ClientDTO::new)
                .collect(Collectors.toList());
    }

    public ClientDTO save(ClientDTO clientDTO) {
        Client client = new Client(clientDTO);
        clientRepository.save(client);
        return new ClientDTO(client);
    }

    public void delete(Long id) {
        clientRepository.deleteById(id);
    }

    public ClientDTO update(ClientDTO clientDTO, Long id) {
        return clientRepository.findById(id)
                .map(client -> {
                    client.setLastName(clientDTO.getLastName());
                    client.setFirstName(clientDTO.getFirstName());
                    return new ClientDTO(clientRepository.save(client));
                })
                .orElseGet(() -> {
                    Client client = new Client(clientDTO);
                    client.setId(id);
                    return new ClientDTO(clientRepository.save(client));
                });
    }

}
