package com.yukon.library.model.book;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class BookExceptionHandler {

    @ResponseBody
    @ExceptionHandler(BookNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String BookNotFoundExceptionHandler(BookNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(BookNotAvailableException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String BookNotAvailableExceptionHandler(BookNotAvailableException ex) {
        return ex.getMessage();
    }
}
