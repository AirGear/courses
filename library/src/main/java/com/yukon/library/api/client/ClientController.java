package com.yukon.library.api.client;


import com.yukon.library.api.book.BookDTO;

import com.yukon.library.model.order.OrderStatus;
import com.yukon.library.model.client.ClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class ClientController {

    @Autowired
    private ClientService service;

    @GetMapping("/clients")
    public ResponseEntity<List<ClientDTO>> all() {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.findAll());
    }

    @GetMapping("/clients/active")
    public ResponseEntity<List<ClientDTO>> getActiveClients() {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.getActiveClients());
    }

    @GetMapping("/clients/passive")
    public ResponseEntity<List<ClientDTO>> getPassiveClients() {

        return ResponseEntity.status(HttpStatus.OK)
        .body(service.getPassiveClients());
    }


    @GetMapping("/clients/{id}")
    public ResponseEntity<ClientDTO> one(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.findById(id));
    }

    @GetMapping("/clients/{id}/books")
    public ResponseEntity<List<BookDTO>> getBooks(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.getBooksByClient(id));
    }
    
    @GetMapping("/clients/{id}/books/active") 
    public ResponseEntity<List<BookDTO>> getActiveBooks(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.getBooksByClient(id, OrderStatus.IN_PROCESS));
    }

    @GetMapping("/clients/{id}/books/finished")
    public ResponseEntity<List<BookDTO>> getFinishedBooks(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.getBooksByClient(id, OrderStatus.FINISHED));
    }

    @PostMapping("/clients/save")
    public ResponseEntity<ClientDTO> newClient(@RequestBody ClientDTO clientDTO) {

        return ResponseEntity.status(HttpStatus.CREATED)
            .body(service.save(clientDTO));
    }


    @PutMapping("/clients/update/{id}")
    public ResponseEntity<ClientDTO> replaceBook(@RequestBody ClientDTO newClient,
                                         @PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.update(newClient, id));
    }

    @DeleteMapping("/clients/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        service.delete(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

}
