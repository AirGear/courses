package com.yukon.library.api.order;

import com.yukon.library.api.book.BookDTO;
import com.yukon.library.api.client.ClientDTO;
import com.yukon.library.model.order.OrderStatus;
import com.yukon.library.model.order.Orders;

import java.util.Objects;


public class OrderDTOGetResponse {

    private BookDTO book;
    private ClientDTO client;

    private OrderStatus status;
    private Long id;

    public OrderDTOGetResponse() {}

    public OrderDTOGetResponse(Orders order) {
        book = new BookDTO(order.getBook());
        client = new ClientDTO(order.getClient());
        status = order.getStatus();
        id = order.getId();
    }

    public OrderDTOGetResponse(BookDTO bookDTO, ClientDTO clientDTO,
                               OrderStatus orderStatus) {

        this.book = bookDTO;
        this.client = clientDTO;
        this.status = orderStatus;
    }

    public BookDTO getBook() {
        return book;
    }

    public ClientDTO getClient() {
        return client;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public Long getId() {
        return id;
    }

    public void setBook(BookDTO book) {
        this.book = book;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderDTOGetResponse that = (OrderDTOGetResponse) o;
        return Objects.equals(book, that.book) &&
                Objects.equals(client, that.client) &&
                status == that.status &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(book, client, status, id);
    }
}
