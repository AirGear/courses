package com.yukon.library.api.order;

import com.yukon.library.model.order.OrderStatus;

import java.util.Objects;

public class OrderDTOSaveRequest {

    private Long customerID;
    private Long bookID;

    private OrderStatus orderStatus;

    public OrderDTOSaveRequest() {
        orderStatus = OrderStatus.IN_PROCESS;
    }

    public OrderDTOSaveRequest(Long customerID, Long bookID, OrderStatus orderStatus) {

        this.customerID = customerID;
        this.orderStatus = orderStatus;
        this.bookID = bookID;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public Long getBookID() {
        return bookID;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public void setBookID(Long bookID) {
        this.bookID = bookID;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderDTOSaveRequest that = (OrderDTOSaveRequest) o;
        return Objects.equals(customerID, that.customerID) &&
                Objects.equals(bookID, that.bookID) &&
                orderStatus == that.orderStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerID, bookID, orderStatus);
    }
}
