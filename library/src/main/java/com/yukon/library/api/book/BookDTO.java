package com.yukon.library.api.book;

import com.yukon.library.model.book.Book;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class BookDTO {

    private Long id;

    private String authorName;
    private String bookName;

    private int copiesCount;

    public BookDTO() {}

    public BookDTO(Book book) {
        this.authorName = book.getAuthorName();
        this.bookName = book.getBookName();
        this.copiesCount = book.getCopiesCount();
        this.id = book.getId();
    }

    public BookDTO(String bookName, String authorName, int copiesCount) {
        this.authorName = authorName;
        this.bookName = bookName;
        this.copiesCount = copiesCount;
    }



    public Long getId() {
        return id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getBookName() {
        return bookName;
    }

    public int getCopiesCount() {
        return copiesCount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setCopiesCount(int copiesCount) {
        this.copiesCount = copiesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDTO bookDTO = (BookDTO) o;
        return copiesCount == bookDTO.copiesCount &&
                id.equals(bookDTO.id) &&
                Objects.equals(authorName, bookDTO.authorName) &&
                Objects.equals(bookName, bookDTO.bookName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, authorName, bookName, copiesCount);
    }
}
