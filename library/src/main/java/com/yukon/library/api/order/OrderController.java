package com.yukon.library.api.order;


import com.yukon.library.model.book.BookNotAvailableException;
import com.yukon.library.model.order.OrderStatus;
import com.yukon.library.model.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderService service;

    @GetMapping("/orders")
    public ResponseEntity<List<OrderDTOGetResponse>> all() {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.findAll());
    }

    @GetMapping("/orders/active")
    public ResponseEntity<List<OrderDTOGetResponse>> activeOrders() {

        return ResponseEntity.status(HttpStatus.OK)
                    .body(service.getOrdersByStatus(OrderStatus.IN_PROCESS));
    }

    @GetMapping("/orders/finished")
    public ResponseEntity<List<OrderDTOGetResponse>> finishedOrders() {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.getOrdersByStatus(OrderStatus.FINISHED));
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<OrderDTOGetResponse> one(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
        .body(service.findById(id));
    }

    @PostMapping("/orders/save")
    public ResponseEntity<OrderDTOGetResponse> newOrder(@RequestBody OrderDTOSaveRequest orderDTO)
            throws BookNotAvailableException {

        return ResponseEntity.status(HttpStatus.CREATED)
            .body(service.save(orderDTO));
    }

    @PutMapping("/orders/update/{id}")
    public ResponseEntity<OrderDTOGetResponse> replaceOrder(@RequestBody OrderDTOSaveRequest newOrder,
                                       @PathVariable Long id) throws BookNotAvailableException {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.update(newOrder, id));
    }

    @PutMapping("/orders/{id}/finish")
    public ResponseEntity<OrderDTOGetResponse> finishOrder(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(service.setStatus(id, OrderStatus.FINISHED));
    }

    @PutMapping("/orders/{id}/activate")
    public ResponseEntity<OrderDTOGetResponse> activateOrder(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(service.setStatus(id, OrderStatus.IN_PROCESS));
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        service.delete(id);
        return ResponseEntity.accepted().build();
    }



}
