package com.yukon.library.api.book;

import com.yukon.library.api.client.ClientDTO;

import com.yukon.library.model.book.BookService;
import com.yukon.library.model.order.OrderStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    @Autowired
    private BookService service;


    @GetMapping("/books")
    public ResponseEntity<List<BookDTO>> all() {

        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findAll());
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<BookDTO> one(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findById(id));
    }

    @GetMapping("/books/{id}/clients")
    ResponseEntity<List<ClientDTO>> getClientsByBook(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getClientsByBook(id));
    }

    @GetMapping("/books/{id}/clients/active")
    ResponseEntity<List<ClientDTO>> getActiveClientsByBook(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getClientsByBook(id, OrderStatus.IN_PROCESS));
    }

    @GetMapping("/books/{id}/clients/passive")
    ResponseEntity<List<ClientDTO>> getPassiveClientsByBook(@PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
            .body(service.getClientsByBook(id, OrderStatus.FINISHED));
    }

    @PostMapping("/books/save")
    public ResponseEntity<BookDTO> newBook(@RequestBody BookDTO book) {

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(service.save(book));
    }


    @PutMapping("/books/update/{id}")
    public ResponseEntity<BookDTO> replaceBook(@RequestBody BookDTO newBook,
                                         @PathVariable Long id) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(service.update(newBook, id));
    }


    @DeleteMapping("/books/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id) {

        service.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
