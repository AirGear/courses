package com.yukon.library.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yukon.library.api.order.OrderController;
import com.yukon.library.api.order.OrderDTOGetResponse;
import com.yukon.library.api.order.OrderDTOSaveRequest;
import com.yukon.library.model.book.Book;
import com.yukon.library.model.client.Client;
import com.yukon.library.model.order.OrderService;
import com.yukon.library.model.order.OrderStatus;
import com.yukon.library.model.order.Orders;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    private Orders order1;
    private Orders order2;
    private Orders order3;

    @Before
    public void init() {
        Book book1 = new Book();
        book1.setId(((long) 1));
        book1.setAuthorName("Steven King");
        book1.setBookName("Dead island");
        book1.setCopiesCount(2);

        Book book2 = new Book();
        book2.setId(2L);
        book2.setAuthorName("Bill Wayne");
        book2.setBookName("Dead island 2");
        book2.setCopiesCount(1);


        Client client1 = new Client();
        client1.setId((1L));
        client1.setFirstName("Alex");
        client1.setLastName("Cambel");

        Client client2 = new Client();
        client2.setLastName("Drobot");
        client2.setFirstName("Andriy");
        client2.setId(2L);

        order1 = new Orders();
        order1.setStatus(OrderStatus.IN_PROCESS);
        order1.setClient(client1);
        order1.setBook(book1);
        order1.setId(1L);

        order2 = new Orders();
        order2.setClient(client2);
        order2.setBook(book1);
        order2.setStatus(OrderStatus.IN_PROCESS);
        order2.setId(2L);

        order3 = new Orders();
        order3.setClient(client1);
        order3.setBook(book2);
        order3.setStatus(OrderStatus.FINISHED);
        order3.setId(3L);

        client1.setOrders(Arrays.asList(order1, order3));
        client2.setOrders(Collections.singletonList(order2));

    }

    @Test
    public void testAll() throws Exception {
        Mockito.when(orderService.findAll()).thenReturn(Stream.of(order1, order2, order3)
                .map(OrderDTOGetResponse::new)
                .collect(Collectors.toList()));

        mockMvc.perform(get("/orders")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].status", is("IN_PROCESS")))

                .andExpect(jsonPath("$[0].client.firstName", is("Alex")))
                .andExpect(jsonPath("$[0].client.lastName", is("Cambel")))

                .andExpect(jsonPath("$[0].book.id", is(1)))
                .andExpect(jsonPath("$[0].book.bookName", is("Dead island")))
                .andExpect(jsonPath("$[0].book.authorName", is("Steven King")))
                .andExpect(jsonPath("$[0].book.copiesCount", is(2)))


                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].status", is("IN_PROCESS")))

                .andExpect(jsonPath("$[1].client.firstName", is("Andriy")))
                .andExpect(jsonPath("$[1].client.lastName", is("Drobot")))

                .andExpect(jsonPath("$[1].book.id", is(1)))
                .andExpect(jsonPath("$[1].book.bookName", is("Dead island")))
                .andExpect(jsonPath("$[1].book.authorName", is("Steven King")))
                .andExpect(jsonPath("$[1].book.copiesCount", is(2)));




    }

    @Test
    public void testOne() throws Exception {

        Mockito.when(orderService.findById(1L)).thenReturn(new OrderDTOGetResponse(order1));

        mockMvc.perform(get("/orders/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.status", is("IN_PROCESS")))

                .andExpect(jsonPath("$.client.firstName", is("Alex")))
                .andExpect(jsonPath("$.client.lastName", is("Cambel")))

                .andExpect(jsonPath("$.book.id", is(1)))
                .andExpect(jsonPath("$.book.bookName", is("Dead island")))
                .andExpect(jsonPath("$.book.authorName", is("Steven King")))
                .andExpect(jsonPath("$.book.copiesCount", is(2)));
    }

    @Test
    public void newOrderTest() throws Exception {

        OrderDTOSaveRequest orderDTOSave = new OrderDTOSaveRequest();
        orderDTOSave.setBookID(1L);
        orderDTOSave.setCustomerID(1L);
        orderDTOSave.setOrderStatus(OrderStatus.IN_PROCESS);

        Mockito.when(orderService.save(orderDTOSave))
                .thenReturn(new OrderDTOGetResponse(order1));

        mockMvc.perform(post("/orders/save")
                .content(new ObjectMapper().writeValueAsString(orderDTOSave))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.status", is("IN_PROCESS")))

                .andExpect(jsonPath("$.client.firstName", is("Alex")))
                .andExpect(jsonPath("$.client.lastName", is("Cambel")))

                .andExpect(jsonPath("$.book.id", is(1)))
                .andExpect(jsonPath("$.book.bookName", is("Dead island")))
                .andExpect(jsonPath("$.book.authorName", is("Steven King")))
                .andExpect(jsonPath("$.book.copiesCount", is(2)));
    }

    @Test
    public void updateOrderTest() throws Exception {

        OrderDTOSaveRequest orderDTOSave = new OrderDTOSaveRequest();
        orderDTOSave.setBookID(1L);
        orderDTOSave.setCustomerID(2L);
        orderDTOSave.setOrderStatus(OrderStatus.IN_PROCESS);

        OrderDTOGetResponse temp = new OrderDTOGetResponse(order2);
        temp.setId(1L);
        Mockito.when(orderService.update(orderDTOSave, 1L))
                .thenReturn(temp);

        mockMvc.perform(put("/orders/update/{id}", 1)
                .content(new ObjectMapper().writeValueAsString(orderDTOSave))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.status", is("IN_PROCESS")))

                .andExpect(jsonPath("$.client.firstName", is("Andriy")))
                .andExpect(jsonPath("$.client.lastName", is("Drobot")))

                .andExpect(jsonPath("$.book.id", is(1)))
                .andExpect(jsonPath("$.book.bookName", is("Dead island")))
                .andExpect(jsonPath("$.book.authorName", is("Steven King")))
                .andExpect(jsonPath("$.book.copiesCount", is(2)));
    }

    @Test
    @Ignore
    public void deleteTest() throws Exception {
        /*Mockito.when(orderService.deleteById(2L)).thenReturn(ResponseEntity
                .status(HttpStatus.ACCEPTED).build());*/
        mockMvc.perform(delete("/orders/delete/{id}", 2))
                .andExpect(status().isAccepted());
    }
}
