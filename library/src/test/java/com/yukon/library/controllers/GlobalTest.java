package com.yukon.library.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yukon.library.api.book.BookDTO;
import com.yukon.library.api.client.ClientDTO;
import com.yukon.library.api.order.OrderDTOSaveRequest;
import com.yukon.library.model.order.OrderStatus;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GlobalTest {

   @Autowired
   private MockMvc mockMvc;


   private BookDTO book1;
   private BookDTO book2;

   private ClientDTO client1;
   private ClientDTO client2;

   private OrderDTOSaveRequest orderDTOSaveRequest;

   @Before
   public void init() {
       book1 = new BookDTO("Dead Island", "Mr. Misha", 4);
       book2 = new BookDTO("Dead Island 2", "Mr. Andr", 2);

       client1 = new ClientDTO("Michel", "Bale");
       client2 = new ClientDTO("Andriy", "Drobot");

       orderDTOSaveRequest = new OrderDTOSaveRequest(1L, 1L, OrderStatus.IN_PROCESS);
   }

   @Test
   public void a_createBookTest() throws Exception {

       mockMvc.perform(post("/books/save")
               .content(new ObjectMapper().writeValueAsString(book1))
               .contentType(MediaType.APPLICATION_JSON)
               .accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())

               .andExpect(jsonPath("$.id", is(1)))
               .andExpect(jsonPath("$.bookName", is("Dead Island")))
               .andExpect(jsonPath("$.authorName", is("Mr. Misha")))
               .andExpect(jsonPath("$.copiesCount", is(4)));
   }



   @Test
    public void b_updateBookTest() throws Exception {
       mockMvc.perform(put("/books/update/{id}", 1)
       .content(new ObjectMapper().writeValueAsString(book2))
               .contentType(MediaType.APPLICATION_JSON)
               .accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())

               .andExpect(jsonPath("$.id", is(1)))
               .andExpect(jsonPath("$.bookName", is("Dead Island 2")))
               .andExpect(jsonPath("$.authorName", is("Mr. Andr")))
               .andExpect(jsonPath("$.copiesCount", is(2)));

   }

   @Test
    public void c_createClient() throws Exception {
       mockMvc.perform(post("/clients/save/")
       .content(new ObjectMapper().writeValueAsString(client1))
               .contentType(MediaType.APPLICATION_JSON)
               .accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())

               .andExpect(jsonPath("$.id", is(1)))
               .andExpect(jsonPath("$.firstName", is("Michel")))
               .andExpect(jsonPath("$.lastName", is("Bale")));
   }

   @Test
    public void d_updateClient() throws  Exception {
       mockMvc.perform(put("/clients/update/{id}", 1)
       .content(new ObjectMapper().writeValueAsString(client2))
               .contentType(MediaType.APPLICATION_JSON)
               .accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())

               .andExpect(jsonPath("$.id", is(1)))
               .andExpect(jsonPath("$.firstName", is("Andriy")))
               .andExpect(jsonPath("$.lastName", is("Drobot")));
   }

   @Test
    public void e_createOder() throws Exception {

       mockMvc.perform(post("/orders/save")
       .content(new ObjectMapper().writeValueAsString(orderDTOSaveRequest))
               .contentType(MediaType.APPLICATION_JSON)
               .accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())

               .andExpect(jsonPath("$.id", is(1)))

               .andExpect(jsonPath("$.book.bookName", is("Dead Island 2")))
               .andExpect(jsonPath("$.book.authorName", is("Mr. Andr")))
               .andExpect(jsonPath("$.book.copiesCount", is(2)))

               .andExpect(jsonPath("$.client.firstName", is("Andriy")))
               .andExpect(jsonPath("$.client.lastName", is("Drobot")));
   }

   @Test
    public void f_getClientsByBook() throws Exception {
       mockMvc.perform(get("/books/{id}/clients", 1))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", hasSize(1)))

               .andExpect(jsonPath("$[0].firstName", is("Andriy")))
               .andExpect(jsonPath("$[0].lastName", is("Drobot")));
   }

   @Test
    public void g_getBooksByClient() throws Exception {
       mockMvc.perform(get("/clients/1/books", 1))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", hasSize(1)))

               .andExpect(jsonPath("$[0].id", is(1)))
               .andExpect(jsonPath("$[0].bookName", is("Dead Island 2")))
               .andExpect(jsonPath("$[0].authorName", is("Mr. Andr")))
               .andExpect(jsonPath("$[0].copiesCount", is(2)));


   }
}
