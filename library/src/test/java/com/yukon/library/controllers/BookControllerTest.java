package com.yukon.library.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yukon.library.api.book.BookController;
import com.yukon.library.api.book.BookDTO;
import com.yukon.library.api.client.ClientDTO;
import com.yukon.library.model.book.Book;
import com.yukon.library.model.book.BookService;
import com.yukon.library.model.client.Client;
import com.yukon.library.model.order.OrderStatus;
import com.yukon.library.model.order.Orders;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)
public class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    private Book book1;
    private Book book2;
    private List<Book> expectedBooks;

    private Client client1;
    private Client client2;

    @Before

    public void init() {
        book1 = new Book();
        book1.setId(((long) 1));
        book1.setAuthorName("Steven King");
        book1.setBookName("Dead island");
        book1.setCopiesCount(2);

        book2 = new Book();
        book2.setId(2L);
        book2.setAuthorName("Bill Wayne");
        book2.setBookName("Dead island 2");
        book2.setCopiesCount(1);

        expectedBooks = new ArrayList<>();
        expectedBooks.add(book1);
        expectedBooks.add(book2);

        client1 = new Client();
        client1.setId((1L));
        client1.setFirstName("Alex");
        client1.setLastName("Cambel");

        client2 = new Client();
        client2.setLastName("Drobot");
        client2.setFirstName("Andriy");
        client2.setId(2L);

        Orders order1 = new Orders();
        order1.setStatus(OrderStatus.IN_PROCESS);
        order1.setClient(client1);
        order1.setBook(book1);

        Orders order2 = new Orders();
        order2.setClient(client2);
        order2.setBook(book1);
        order2.setStatus(OrderStatus.IN_PROCESS);

        ArrayList<Orders> orders = new ArrayList<>();

        orders.add(order1);
        orders.add(order2);

        book1.setOrders(orders);

    }

    @Test
    public void testAll() throws Exception {
        Mockito.when(bookService.findAll()).thenReturn(expectedBooks.stream()
        .map(BookDTO::new)
        .collect(Collectors.toList()));

        mockMvc.perform(get("/books")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].bookName", is("Dead island")))
                .andExpect(jsonPath("$[0].authorName", is("Steven King")))
                .andExpect(jsonPath("$[0].copiesCount", is(2)))

                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].bookName", is("Dead island 2")))
                .andExpect(jsonPath("$[1].authorName", is("Bill Wayne")))
                .andExpect(jsonPath("$[1].copiesCount", is(1)));
    }

    @Test
    public void testOne() throws Exception {

        Mockito.when(bookService.findById(1L)).thenReturn(new BookDTO(book1));

        mockMvc.perform(get("/books/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.bookName", is("Dead island")))
                .andExpect(jsonPath("$.authorName", is("Steven King")))
                .andExpect(jsonPath("$.copiesCount", is(2)));
    }

    @Test
    public void testGetClients() throws Exception {
        Mockito.when(bookService.getClientsByBook(1L))
                .thenReturn(Arrays.asList(new ClientDTO(client1), new ClientDTO(client2)));

        mockMvc.perform(get("/books/1/clients")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].firstName", is("Alex")))
                .andExpect(jsonPath("$[0].lastName", is("Cambel")))

                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].firstName", is("Andriy")))
                .andExpect(jsonPath("$[1].lastName", is("Drobot")));
    }

    @Test
    public void newBookTest() throws Exception {

        Mockito.when(bookService.save(new BookDTO(book1)))
                .thenReturn(new BookDTO(book1));

        mockMvc.perform(post("/books/save")
            .content(new ObjectMapper().writeValueAsString(new BookDTO(book1)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.bookName", is("Dead island")))
                .andExpect(jsonPath("$.authorName", is("Steven King")))
                .andExpect(jsonPath("$.copiesCount", is(2)));

    }

    @Test
    public void updateBookTest() throws Exception {

        BookDTO temp = new BookDTO(book2);
        temp.setId(1L);

        Mockito.when(bookService.update(new BookDTO(book2), 1L))
                .thenReturn(temp);

        mockMvc.perform(put("/books/update/{id}", 1)
                .content(new ObjectMapper().writeValueAsString(new BookDTO(book2)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.bookName", is("Dead island 2")))
                .andExpect(jsonPath("$.authorName", is("Bill Wayne")))
                .andExpect(jsonPath("$.copiesCount", is(1)));
    }

    @Test
    @Ignore
    public void deleteTest() throws Exception {
        /*Mockito.when(bookService.deleteById(2L)).thenReturn(ResponseEntity
                .status(HttpStatus.ACCEPTED).build());*/
        mockMvc.perform(delete("/books/delete/{2}", 2L))
                .andExpect(status().isAccepted());
    }

}
