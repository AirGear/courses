package com.yukon.library.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yukon.library.api.book.BookDTO;
import com.yukon.library.api.client.ClientController;
import com.yukon.library.api.client.ClientDTO;
import com.yukon.library.model.book.Book;
import com.yukon.library.model.client.Client;
import com.yukon.library.model.client.ClientService;
import com.yukon.library.model.order.OrderStatus;
import com.yukon.library.model.order.Orders;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientController.class)
public class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService clientService;

    private Book book1;

    private Client client1;
    private Client client2;

    @Before
    public void init() {
        book1 = new Book();
        book1.setId(((long) 1));
        book1.setAuthorName("Steven King");
        book1.setBookName("Dead island");
        book1.setCopiesCount(2);


        client1 = new Client();
        client1.setId((1L));
        client1.setFirstName("Alex");
        client1.setLastName("Cambel");

        client2 = new Client();
        client2.setLastName("Drobot");
        client2.setFirstName("Andriy");
        client2.setId(2L);

        Orders order1 = new Orders();
        order1.setStatus(OrderStatus.IN_PROCESS);
        order1.setClient(client1);
        order1.setBook(book1);

        Orders order2 = new Orders();
        order2.setClient(client2);
        order2.setBook(book1);
        order2.setStatus(OrderStatus.IN_PROCESS);


        client1.setOrders(Collections.singletonList(order1));
        client2.setOrders(Collections.singletonList(order2));

    }

    @Test
    public void testAll() throws Exception {
        Mockito.when(clientService.findAll()).thenReturn(Stream.of(client1, client2)
                .map(ClientDTO::new)
                .collect(Collectors.toList()));

        mockMvc.perform(get("/clients")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].firstName", is("Alex")))
                .andExpect(jsonPath("$[0].lastName", is("Cambel")))

                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].firstName", is("Andriy")))
                .andExpect(jsonPath("$[1].lastName", is("Drobot")));
    }

    @Test
    public void testOne() throws Exception {

        Mockito.when(clientService.findById(1L)).thenReturn(new ClientDTO(client1));

        mockMvc.perform(get("/clients/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.firstName", is("Alex")))
                .andExpect(jsonPath("$.lastName", is("Cambel")));
    }

    @Test
    public void testGetBooks() throws Exception {
        Mockito.when(clientService.getBooksByClient(1L))
                .thenReturn(Collections.singletonList(new BookDTO(book1)));

        mockMvc.perform(get("/clients/1/books")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].bookName", is("Dead island")))
                .andExpect(jsonPath("$[0].authorName", is("Steven King")))
                .andExpect(jsonPath("$[0].copiesCount", is(2)));
    }

    @Test
    public void newClientTest() throws Exception {

        Mockito.when(clientService.save(new ClientDTO(client1)))
                .thenReturn(new ClientDTO(client1));

        mockMvc.perform(post("/clients/save")
                .content(new ObjectMapper().writeValueAsString(new ClientDTO(client1)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.firstName", is("Alex")))
                .andExpect(jsonPath("$.lastName", is("Cambel")));
    }

    @Test
    public void updateBookTest() throws Exception {

        ClientDTO temp = new ClientDTO(client2);
        temp.setId(1L);

        Mockito.when(clientService.update(new ClientDTO(client2), 1L))
                .thenReturn(temp);

        mockMvc.perform(put("/clients/update/{id}", 1)
                .content(new ObjectMapper().writeValueAsString(new ClientDTO(client2)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.firstName", is("Andriy")))
                .andExpect(jsonPath("$.lastName", is("Drobot")));
    }

    @Test
    @Ignore
    public void deleteTest() throws Exception {
        /*Mockito.when(clientService.deleteById(2L)).thenReturn(ResponseEntity
                .status(HttpStatus.ACCEPTED).build());*/
        mockMvc.perform(delete("/clients/delete/{2}", 2L))
                .andExpect(status().isAccepted());
    }

}
