package com.yukon.library.service;

import com.yukon.library.model.book.Book;
import com.yukon.library.api.book.BookDTO;
import com.yukon.library.model.book.BookRepository;
import com.yukon.library.model.client.Client;
import com.yukon.library.api.client.ClientDTO;
import com.yukon.library.model.order.OrderStatus;
import com.yukon.library.model.order.Orders;
import com.yukon.library.model.book.BookService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
public class BookServiceTest {
    @InjectMocks
    private BookService service;

    @Mock
    private BookRepository repository;

    private Book book1;
    private Book book2;
    private List<Book> expectedBooks;

    private Client client1;
    private Client client2;

    private Orders order1;
    private Orders order2;

    ArrayList<Orders> orders;

    @Before
    public void init() {
        book1 = new Book ();
        book1.setId(((long) 1));
        book1.setAuthorName("Steven King");
        book1.setBookName("Dead island");
        book1.setCopiesCount(2);

        book2 = new Book();
        book2.setId(((long) 2));
        book2.setAuthorName("Bill Wayne");
        book2.setBookName("Dead island 2");
        book2.setCopiesCount(1);

        expectedBooks = new ArrayList<>();
        expectedBooks.add(book1);
        expectedBooks.add(book2);

        client1 = new Client();
        client1.setId((long)1);
        client1.setFirstName("Alex");
        client1.setLastName("Cambel");

        client2 = new Client();
        client2.setLastName("Drobot");
        client2.setFirstName("Andriy");
        client2.setId((long)2);

        order1 = new Orders();
        order1.setStatus(OrderStatus.IN_PROCESS);
        order1.setClient(client1);
        order1.setBook(book1);

        order2 = new Orders();
        order2.setClient(client2);
        order2.setBook(book1);
        order2.setStatus(OrderStatus.IN_PROCESS);

       orders = new ArrayList<>();

        orders.add(order1);
        orders.add(order2);

        book1.setOrders(orders);

    }


    @Test
    public void testGetBookById() {
        Mockito.when(repository.findById((long)1)).thenReturn(java.util.Optional.of(book1));
        assertThat(service.findById((long) 1), equalTo(new BookDTO(book1)));
    }

    @Test
    public void testFindAll() {
        Mockito.when(repository.findAll()).thenReturn(expectedBooks);
        assertThat(service.findAll(), equalTo(expectedBooks.stream()
                .map(BookDTO::new)
                .collect(Collectors.toList())));
    }

    @Test
    public void testSave() {
        Mockito.when(repository.save(book1)).thenReturn(book1);
        assertThat(service.save(new BookDTO(book1)), equalTo(new BookDTO(book1)));
    }

    @Test
    public void testUpdate() {
        book2.setId((long)1);
        Mockito.when(repository.findById((long)1)).thenReturn(java.util.Optional.ofNullable(book1));
        Mockito.when(repository.save(book2)).thenReturn(book2);

        assertThat(service.update(new BookDTO(book2),(long)1),equalTo(new BookDTO(book2)));

    }

    @Test
    public void testGetClientsByBook() {
        Mockito.when(repository.findById((long) 1)).thenReturn(java.util.Optional.ofNullable(book1));
        assertThat(service.getClientsByBook((long) 1), equalTo(orders.stream()
        .map(order -> new ClientDTO(order.getClient()))
        .collect(Collectors.toList())));
    }


    @Test
    public void testIsBookAvailable() {
        assertThat(service.isAvailable(book1), equalTo(false));

    }



}
