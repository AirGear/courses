package com.yukon.payroll;

public class OrderNotFoundException extends RuntimeException {
    public OrderNotFoundException(Long id) {
        super("Can not fond order " + id);
    }
}
