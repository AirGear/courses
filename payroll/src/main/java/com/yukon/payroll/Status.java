package com.yukon.payroll;

public enum Status {
    IN_PROGRESS,
    COMPLETED,
    CANCELED
}
